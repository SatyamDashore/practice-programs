interface Point {
    x: number,
    y: number,
    drawPoint: () =>void,
    getDistance: () =>void
}

let drawPoint = (point: Point) => {
    // ...
}

let getDistance = (pointA:Point,pointB:Point) => {
    // ...
}