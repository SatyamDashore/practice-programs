class Point {
    // // Fields
    // private x: number;
    // private y: number;

    // // Constructor

    // constructor(x: number,y: number) {
    //     this.x = x;
    //     this.y = y;
    // }

    // The above code acan be replaced by:
    constructor(private x:number, private y:m=number) {
    }

    // Methods
    drawPoint() {
        console.log('X: ' + this.x + ', Y: ' + this.y);
    }

    get X() {return this.x;}

    set X(value) {
        if(value< 0)
            throw new Error('Value cannot be less than 0.');
        this.X = value;
    }
}

let point = new Point(1,4);
let x = point.X;   // getter property
point.X = 10;  // setter property
point.drawPoint()
