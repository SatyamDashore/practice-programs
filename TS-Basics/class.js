var Point = /** @class */ (function () {
    function Point() {
    }
    // Methods
    Point.prototype.drawPoint = function () {
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point.prototype.getDistance = function () {
        // ...
    };
    return Point;
}());
var point = new Point();
point.x = 1;
point.y = 4;
point.drawPoint();
