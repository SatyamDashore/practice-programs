import json
import urllib
import boto3
def lambda_handler(event, context):
    # Get the bucket id
    print(event)
    bucket = event['Records'][0]['s3']['bucket']['name']
    
    # Get the file/key name
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding = 'utf-8')
    
    try:
        # Fetch the file from s3
        import boto3
        s3 = boto3.client('s3')
        response = s3.get_object(Bucket = bucket, Key= key)
        
        # Deserialize the files's content
        text = response['Body'].read().decode()
        data = json.loads(text)
        
        print(data)
        
        transactions = data['Transactions']
        for record in transactions:
            print(record['transType'])
        return ('Success')
        
    except Exception as e:
        print(e)
        raise e


# Event Json:

# {
#     "Transactions": [
#         {
#             "transType":"PURCHASE",
#             "amount":45.07
#         },
#         {
#             "transType":"REFUND",
#             "amount":39.43
#         },
#         {
#             "transType":"RE-FUND",
#             "amount":334.34
#         }
#     ]
# }