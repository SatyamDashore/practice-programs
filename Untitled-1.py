from datetime import datetime, timedelta
import os, csv
from Transaction_Object import *

def json_to_csv(all_transactions):
    filename = '{base}{path}_transactionObject_csv'.format(base=os.environ['PWD'], path='/')
    # Open csv file for writing
    data_file = open(filename, 'w+')       
 
    # create the csv writer object
    csv_writer = csv.writer(data_file)
    csv_writer.writerow(['today','ref_id','currency','amount'])       #field_names
    # framing the data into required csv format
    if not all_transactions[0].get("exchangeDetails",False): # When the transactions are coming from Synapse
        for transaction in all_transactions: 
            t = []
            t.append(datetime.fromtimestamp(transaction["recent_status"]["date"]["$date"]/1000))
            t.append(transaction["_id"]["$oid"])
            t.append(transaction["amount"]["currency"])
            t.append(transaction["amount"]["amount"])
            csv_writer.writerow(t)
    else:                                                   # When the transactions are coming from Wise
        for transaction in all_transactions: 
            t = []
            t.append(transaction["date"].replace("T"," ").replace("Z",""))
            t.append(transaction['referenceNumber'])
            t.append(transaction["amount"]["currency"])
            t.append(transaction["amount"]["value"])
            csv_writer.writerow(t)
    data_file.close()
        
    fp = open(filename,"rb")
    Transactions_csv = fp.read()
    os.remove(os.path.realpath(filename))
    fp.close()
    return Transactions_csv


s = json_to_csv(synapse_all_transactions)

s = json_to_csv(Wise_all_transactions)
print(s)