import memory_profiler as mem_profile
import random
import time
names=['Satyam','Akshat','Aman','Sumeet','Ritik','Pratik']
branch=['CS','IT','CIVIL','MECH','EC','BIOTECH']

print("Memory(Before): {} Mb".format(mem_profile.memory_usage()))

def people_list(n):
    result=[]
    for i in range(n):
        person = {
            'id':i,
            'name':random.choice(names),
            'branch':random.choice(branch)
        }
        result.append(person)
    return result

def people_generator(n):
    for i in range(n):
        person = {
            'id':i,
            'name':random.choice(names),
            'branch':random.choice(branch)
        }
        yield (person)

t1 = time.time()
#people = people_list(1000000)
people = people_generator(1000000)
t2 = time.time() - t1

print("Memory(After): {} Mb".format(mem_profile.memory_usage()))
print("Time taken: {} sec".format(t2))
