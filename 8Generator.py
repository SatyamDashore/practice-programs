def square_nums(nums):
    for i in nums:
        yield (i*i)

num = [1,2,3,4,5]
squares = square_nums(num)
"""
while(True):         # This will return an error wherever next(squares) gets out pof index and print StopIteration
    print(next(squares))
"""
for i in squares:
    print(i)