class decorator_class(object):
    
    def __init__(self, orginial_function):
        self.original_function = orginial_function

    def __call__(self, *args, **kwargs):
        print("'__call__' method executed before {}".format(self.original_function.__name__))
        return self.original_function(*args, **kwargs)

@decorator_class
def my_func():
    print("This is my_func")

@decorator_class
def display_info(name,age):
    print("'display_info' ran with arguments ({}, {})".format(name, age))

my_func()
print()
display_info('Satyam',21)