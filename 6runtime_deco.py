import time
def runtime(org_func):
    
    def wrapper(*args, **kwargs):
        start = time.time()
        result = org_func(*args, **kwargs)
        timeTaken= time.time() - start
        print("{} ran in: {} sec".format(org_func.__name__, timeTaken))
        return result
    return wrapper

@runtime
def display_info(name,age):
    print("'display_info' ran with arguments ({}, {})".format(name, age))

display_info("Satyam",21)