from time import sleep

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger


def foo(bar):
    print(bar)


def main():
    scheduler = BackgroundScheduler()
    scheduler.start()

    trigger = CronTrigger(
        year="*", month="*", day="*", hour="0", minute="0", second="5"
    )
    scheduler.add_job(
        foo,
        trigger=trigger,
        args=["hello world"],
        name="daily foo",
    )
    while True:
        sleep(5)


if __name__ == "__main__":
    main()


