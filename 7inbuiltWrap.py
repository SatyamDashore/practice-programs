from functools import wraps

def my_logger(org_func):
    import logging
    logging.basicConfig(filename="{}.log".format(org_func.__name__),level=logging.INFO)

    @wraps(org_func)
    def wrapper(*args, **kwargs):
        logging.info(
            'Ran with args: {}, and kwargs: {}'.format(args, kwargs))
        return org_func(*args, **kwargs)
    return wrapper

def runtime(org_func):
    import time
    @wraps(org_func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = org_func(*args, **kwargs)
        timeTaken= time.time() - start
        print("{} ran in: {} sec".format(org_func.__name__, timeTaken))
        return result
    return wrapper

@my_logger
@runtime
def display_info(name,age):
    print("'display_info' ran with arguments ({}, {})".format(name, age))

display_info("Sumeet",23)