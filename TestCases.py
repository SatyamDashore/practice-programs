@elasticapm.capture_span()
def update_node(id,payload,view,dehydrate,updated_by="SELF",run_async=False,creating_card=False):
    from tasks_short import async_add_log, async_send_notification
    nodes = Nodes()
    node_obj = nodes.one({"_id":id},'LOGS_RAW')
    old_node_obj = node_obj
    if node_obj["type"] in ["CARD-US","SUBCARD-US"]:
        card_preferences = get_card_preferences(node_type=node_obj["type"],
            document_id=node_obj["info"]["document_id"],
            document=False,
            rules=False,
            cip_tag=False,
            user_account_type=False,
            user_id=node_obj["user_id"])
    else:
        card_preferences = False
    node_obj = add_misc_info(nodes,node_obj,payload,card_preferences)
    node_obj = add_client_info(nodes,node_obj,payload)
    node_obj = add_info_info(nodes,node_obj,payload,True,False,False,card_preferences,creating_card)
    node_obj = add_extra_info(nodes,node_obj,payload)
    node_obj = add_timeline_info(nodes,node_obj,payload)
    attempt_update_card_limits = False
    if "info" in payload and "credit_limit" in payload["info"]:
        if node_obj["info"]["loan_type"] in ["REVOLVING"]:
            node_obj = add_agreement_info(node_obj,payload)
            node_obj = do_loan_decisioning(node_obj,payload.get("preview_only",False),generate_agreement=True)
            attempt_update_card_limits = True
        else:
            raise BadRequest("cannot_patch_one_time_credit_limit -- Cannot patch credit limit for {0} loans.".format(node_obj["info"]["loan_type"]))
    node_obj = nodes.patch(node_obj,view,dehydrate)
    nodeid = get_object_id(node_obj)
    if run_async:
        async_add_log.send(nodeid,updated_by,"NODE|PATCH")
    else:
        async_add_log(nodeid,updated_by,"NODE|PATCH")
    # if attempt_update_card_limits: update_card_limits(nodeid)
    old_node_obj = nodes._dehydrate(old_node_obj, "")
    if run_async:
        async_send_notification.send(node=node_obj,old_node=old_node_obj,payload=payload)
    else:
        send_notification(node=node_obj,old_node=old_node_obj,payload=payload)
    return node_obj

#mock.py -->

def mock_get_card_preferences(node_type,document_id,document,rules,cip_tag,user_account_type,user_id):
    preferences = {
        'allow_foreign_transactions': False,
        'atm_deposit_limit': 0,
        'atm_withdrawal_limit': 1000,
        'instant_activate': False,
        'max_pin_attempts': 3,
        'pos_withdrawal_limsit': 1000,
        'security_alerts': True}
    return preferences


def mock_check_client_node_controls(payload,skip_node_count_validation,skip_node_type_validation):
    return {
        'bank_code': 'SYN',
        'card_preferences': False,
        'client_descriptor': 'Nisha Karki',
        'interchange_client_sub_id': None,
        'skip_check_address_validation': False
    }


def mock_object_type_check(type, mock_create_node_obj):
    if type == 'REPAY-US':
        return mock_create_node_obj

    if type == 'ACH-US':
        mock_create_node_obj["type"] = 'ACH-US'
        mock_create_node_obj["info"]["account_num"] = '78687586589'
        mock_create_node_obj["info"]["routing_num"] = '2453455'
        return mock_create_node_obj

    if type == 'RPPS-US':
        mock_create_node_obj["type"] = 'RPPS-US'
        mock_create_node_obj["info"]["account_num"] = '78687586589'
        mock_create_node_obj["info"]["biller_id"] = '2453455'
        return mock_create_node_obj
    
    if type == 'FPS-UK':
        mock_create_node_obj["type"] = 'FPS-UK'
        mock_create_node_obj["info"]["account_num"] = '78687586589'
        mock_create_node_obj["info"]["sort_code"] = '2453455'
        return mock_create_node_obj
    
    if type == 'SEPA-EU':
        mock_create_node_obj["type"] = 'SEPA-EU'
        mock_create_node_obj["info"]["iban"] = '2453455'
        mock_create_node_obj['is_active'] = False
        mock_create_node_obj['extra']['other']['micro']='128'
        mock_create_node_obj['extra']['other']['micro_meta']='128'
        return mock_create_node_obj


def mock_default_node_allowed(payload):
    mock_create_node_obj = {
        'allowed': 'CREDIT',
        'extra': {'note': 'INCOME|INSUFFICIENT',
                'other': {'reset_meta': {'last_reset': datetime.datetime(2022, 1, 26, 14, 20, 25, 641033),
                                            'reset_attempts': 1}}},
        'info': {'bank_code': False,
                'credit_limit': {'amount': 10000},
                'document_id': '2a4a5957a3a62aaac1a0dd0edcae96ea2cdee688ec6337b20745eed8869e3ac8',
                'loan_type': 'REVOLVING',
                'nickname': 'My Deposit Account',
                'schedule': []},
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }
    return mock_object_type_check(payload['type'], mock_create_node_obj)
    

def mock_create_matches(payload):
    mock_create_node_obj = {
        'allowed': 'REJECTED',
        'extra': {'note': 'INCOME|INSUFFICIENT',
                'other': {'reset_meta': {'last_reset': datetime.datetime(2022, 1, 26, 14, 34, 41, 859544),
                                            'reset_attempts': 1}}},
        'info': {'bank_code': 'SYN',
                'credit_limit': {'amount': 10000},
                'document_id': '2a4a5957a3a62aaac1a0dd0edcae96ea2cdee688ec6337b20745eed8869e3ac8',
                'loan_type': 'REVOLVING',
                'nickname': 'My Deposit Account',
                'schedule': []},
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')}

    return mock_object_type_check(payload['type'], mock_create_node_obj)


def mock_default_object(self,type,loan_type=False):
    mock_create_node_obj = {
        
        '_v': 2,
        'allowed': 'INACTIVE',
        'allowed_status_code': None,
        'client': {'id': 0, 'name': None},
        'extra': {'note': None,
                'other': {'created_on': datetime.datetime(2022, 1, 26, 14, 38, 22, 232444), 'residual_interest': 0},
                'supp_id': None},
        'info': {'agreements': [],
                'balance': {'amount': 0, 'currency': 'cUSD'},
                'bank_code': 'EBT',
                'credit_limit': {'amount': 0, 'currency': 'cUSD'},
                'document_id': None,
                'interest': {'accrued': 0, 'apr': 0, 'cap': 0},
                'loan_type': 'REVOLVING',
                'monthly_withdrawals_remaining': None,
                'nickname': '01551069639418252376',
                'reserve_node_id': None,
                'schedule': None,
                'split_ratio': None},
        'is_active': True,
        'timeline': [{'date': datetime.datetime(2022, 1, 26, 14, 38, 22, 230466), 'note': 'Node created.'}],
        'type': 'REPAY-US',
        'user_id': None}

    return mock_object_type_check(type, mock_create_node_obj)


def mock_do_external_calls_and_convert(payload,type,interchange_client_sub_id,client_descriptor,do_cards=False):
    mock_create_node_obj = {
        
        'allowed': 'REJECTED',
        'extra': {'note': 'INCOME|INSUFFICIENT',
                'other': {'reset_meta': {'last_reset': datetime.datetime(2022, 1, 26, 14, 34, 41, 859544),
                                            'reset_attempts': 1}}},
        'info': {'bank_code': 'SYN',
                'credit_limit': {'amount': 10000},
                'document_id': '2a4a5957a3a62aaac1a0dd0edcae96ea2cdee688ec6337b20745eed8869e3ac8',
                'loan_type': 'REVOLVING',
                'nickname': 'My Deposit Account',
                'schedule': []},
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }

    return mock_object_type_check(payload['type'], mock_create_node_obj)


def mock_add_misc_info(nodes,node_obj,payload,card_preferences):
    mock_create_node_obj = {
        
        '_v': 2,
        'allowed': 'REJECTED',
        'allowed_status_code': None,
        'client': {'id': 0, 'name': None},
        'extra': {'note': None,
                'other': {'created_on': datetime.datetime(2022, 1, 26, 14, 38, 22, 232444), 'residual_interest': 0},
                'supp_id': None},
        'info': {'agreements': [],
                'balance': {'amount': 0, 'currency': 'cUSD'},
                'bank_code': 'EBT',
                'credit_limit': {'amount': 0, 'currency': 'cUSD'},
                'document_id': None,
                'interest': {'accrued': 0, 'apr': 0, 'cap': 0},
                'loan_type': 'REVOLVING',
                'monthly_withdrawals_remaining': None,
                'nickname': '01551069639418252376',
                'reserve_node_id': None,
                'schedule': None,
                'split_ratio': None},
        'is_active': True,
        'timeline': [{'date': datetime.datetime(2022, 1, 26, 14, 38, 22, 230466), 'note': 'Node created.'}],
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }

    return mock_object_type_check(payload['type'], mock_create_node_obj)


def mock_add_client_info(nodes,node_obj,payload):
    mock_create_node_obj = {
        
        '_v': 2,
        'allowed': 'REJECTED',
        'allowed_status_code': None,
        'client': {'id': 0, 'name': None},
        'extra': {'note': None,
                'other': {'created_on': datetime.datetime(2022, 1, 26, 14, 38, 22, 232444), 'residual_interest': 0},
                'supp_id': None},
        'info': {'agreements': [],
                'balance': {'amount': 0, 'currency': 'cUSD'},
                'bank_code': 'EBT',
                'credit_limit': {'amount': 0, 'currency': 'cUSD'},
                'document_id': None,
                'interest': {'accrued': 0, 'apr': 0, 'cap': 0},
                'loan_type': 'REVOLVING',
                'monthly_withdrawals_remaining': None,
                'nickname': '01551069639418252376',
                'reserve_node_id': None,
                'schedule': None,
                'split_ratio': None},
        'is_active': True,
        'timeline': [{'date': datetime.datetime(2022, 1, 26, 14, 38, 22, 230466), 'note': 'Node created.'}],
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }

    return mock_object_type_check(payload['type'], mock_create_node_obj)


def mock_add_info_info(nodes,node_obj,payload,validate_routing=True,creating=True,skip_check_address_validation=False,card_preferences= False,creating_card=False):
    mock_create_node_obj = {
        
        '_v': 2,
        'allowed': 'REJECTED',
        'allowed_status_code': None,
        'client': {'id': 0, 'name': None},
        'extra': {'note': None,
                'other': {'created_on': datetime.datetime(2022, 1, 26, 14, 38, 22, 232444),
                            'residual_interest': 0},
                'supp_id': None},
        'info': {'agreements': [],
                'balance': {'amount': 0, 'currency': 'cUSD'},
                'bank_code': 'SYN',
                'credit_limit': {'amount': 10000},
                'document_id': '2a4a5957a3a62aaac1a0dd0edcae96ea2cdee688ec6337b20745eed8869e3ac8',
                'interest': {'accrued': 0, 'apr': 0, 'cap': 0},
                'loan_type': 'REVOLVING',
                'monthly_withdrawals_remaining': None,
                'nickname': 'My Deposit Account',
                'reserve_node_id': None,
                'schedule': [],
                'split_ratio': None},
        'is_active': True,
        'timeline': [{'date': datetime.datetime(2022, 1, 26, 14, 38, 22, 230466),
                    'note': 'Node created.'}],
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }

    return mock_object_type_check(payload['type'], mock_create_node_obj)


def mock_add_extra_info(nodes,node_obj,payload):
    mock_create_node_obj = {
        
        '_v': 2,
        'allowed': 'REJECTED',
        'allowed_status_code': None,
        'client': {'id': 0, 'name': None},
        'extra': {'note': 'INCOME|INSUFFICIENT',
                'other': {'created_on': datetime.datetime(2022, 1, 26, 14, 38, 22, 232444),
                            'reset_meta': {'last_reset': datetime.datetime(2022, 1, 26, 14, 34, 41, 859544),
                                            'reset_attempts': 1},
                            'residual_interest': 0},
                'supp_id': None},
        'info': {'agreements': [],
                'balance': {'amount': 0, 'currency': 'cUSD'},
                'bank_code': 'SYN',
                'credit_limit': {'amount': 10000},
                'document_id': '2a4a5957a3a62aaac1a0dd0edcae96ea2cdee688ec6337b20745eed8869e3ac8',
                'interest': {'accrued': 0, 'apr': 0, 'cap': 0},
                'loan_type': 'REVOLVING',
                'monthly_withdrawals_remaining': None,
                'nickname': 'My Deposit Account',
                'reserve_node_id': None,
                'schedule': [],
                'split_ratio': None},
        'is_active': True,
        'timeline': [{'date': datetime.datetime(2022, 1, 26, 14, 38, 22, 230466),
                    'note': 'Node created.'}],
        'type': 'REPAY-US',
        'user_id': ObjectId('61a92b2e758fbf0b42bdfc01')
    }

    return mock_object_type_check(payload['type'], mock_create_node_obj)


@pytest.fixture
def fixture_update_node(mocker:MockerFixture):
    mocker.patch('orm.Nodes.__init__', return_value=None)
    mocker.patch('orm.Nodes.one', return_value=mock_node_obj_6)
    mocker.patch('data.get_card_preferences', mock_get_card_preferences)
    mocker.patch('data.add_misc_info', return_value=mock_data_misc_info)
    mocker.patch('data.add_info_info', return_value=mock_data_add_info)
    mocker.patch('data.add_agreement_info', return_value=mock_data_add_agreement_info)
    mocker.patch('data.do_loan_decisioning', return_value=mock_data_do_loan_decisioning)
    mocker.patch('orm.Nodes.patch', return_value={'_id': {'$oid': '61a92c435d5fe754ce3b41ed'}, 'info': {'nickname': 'Node ID ending with 3b41ed'}, 'type': 'CARD-US'})
    mocker.patch('tasks_short.async_add_log', return_value=mock_tasks_async_add_log)
    mocker.patch('orm.Nodes._dehydrate', return_value=mock_nodes_dehydrate)
    mocker.patch('tasks_short.async_send_notification.send', return_value=True)


@pytest.fixture
def fixture_update_node_cover_else(mocker:MockerFixture):
    mocker.patch('orm.Nodes.__init__', return_value=None)
    mocker.patch('orm.Nodes.one', return_value=mock_node_obj_4)
    mocker.patch('data.add_misc_info', return_value=mock_node_obj_4)
    mocker.patch('data.add_info_info', return_value=mock_node_obj_4)


#Test.py -->

def test_update_node_if(fixture_update_node):
    payload = {
        "timeline_obj":{"note":"Card reset."},
        "info": {
            "credit_limit": 10000,
        },
        "extra":{
            "other": {"reset_meta":{"reset_attempts": 1, "last_reset": datetime.datetime.utcnow()}}
        }
    }
    expected = {
        '_id': {'$oid': '61a92c435d5fe754ce3b41ed'},
        'info': {'nickname': 'Node ID ending with 3b41ed'},
        'type': 'CARD-US'
    }

    actual = data.update_node(ObjectId('61a92c435d5fe754ce3b41ed'),payload,"PUBLIC",True,updated_by="SELF")
    assert expected == actual

    actual = data.update_node(ObjectId('61a92c435d5fe754ce3b41ed'),payload,"PUBLIC",True,updated_by="SELF",run_async=True)
    assert expected == actual

def test_update_node_else(fixture_update_node_cover_else):
    payload = {
        "timeline_obj":{"note":"Card reset."},
        "info": {
            "credit_limit": 10000,
        },
        "extra":{
            "other": {"reset_meta":{"reset_attempts": 1, "last_reset": datetime.datetime.utcnow()}}
        }
    }

    with pytest.raises(errors.BadRequest):
        data.update_node(ObjectId('61a92c435d5fe754ce3b41ed'),payload,"PUBLIC",True,updated_by="SELF")



