"""
def decorator_func(org_func):
    def wrapper_func():
        return org_func()
    return wrapper_func

def my_func():
    print("This is my_func")
    
decorated_func=decorator_func(my_func)
decorated_func()

"""
# Ouptut--> This is my_func

def decorator_func(org_func):
    def wrapper_func():
        print("wrapper executed before {}".format(org_func.__name__))
        return org_func()
    return wrapper_func

"""
def my_func():
    print("This is my_func")
    
my_func = decorator_func(my_func)
my_func()"""

# Output--->
"""
wrapper executed before my_func
This is my_func
"""
    
@decorator_func
def my_func():
    print("This is my_func")

my_func()

# Output--->
"""
wrapper executed before my_func
This is my_func
"""

