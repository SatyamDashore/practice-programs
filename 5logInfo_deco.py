
def my_logger(org_func):
    import logging
    logging.basicConfig(filename="{}.log".format(org_func.__name__),level=logging.INFO)

    def wrapper(*args, **kwargs):
        logging.info(
            'Ran with args: {}, and kwargs: {}'.format(args, kwargs))
        return org_func(*args, **kwargs)
    return wrapper

@my_logger
def display_info(name,age):
    print("'display_info' ran with arguments ({}, {})".format(name, age))

display_info("Satyam",21)
