import boto3
AWS_REGION = "ap-south-1"

client = boto3.client("s3", region_name=AWS_REGION)

bucket_name = "demo-bucket-23253321"
location = {'LocationConstraint': AWS_REGION}

# CREATING S3 BUCKET....
# response = client.create_bucket(Bucket=bucket_name, CreateBucketConfiguration=location)

# print("Amazon S3 bucket has been created")

#LISTING S3 BUCKETS....

# response = client.list_buckets()

# print("Listing Amazon S3 Buckets:")

# for bucket in response['Buckets']:
#     print(f"-- {bucket['Name']}")

#DELETING A S3 BUCKET....

# client.delete_bucket(Bucket=bucket_name)

# print("Amazon S3 Bucket has been deleted")

# UPLOADING TO S3 BUCKET....
file_name = "Transactions.json"
client.upload_file(file_name, bucket_name,'New_Transactions.json', ExtraArgs=None)
print(f"'{file_name}' has been uploaded to '{bucket_name}'")
