def decorator_func(org_func):
    def wrapper_func(*args,**kwargs):
        print("wrapper executed before {}".format(org_func.__name__))
        return org_func(*args,**kwargs)
    return wrapper_func

@decorator_func
def my_func():
    print("This is my_func")

@decorator_func
def display_info(name,age):
    print("'display_info' ran with arguments ({}, {})".format(name, age))

my_func()
print()
display_info('Satyam',21)