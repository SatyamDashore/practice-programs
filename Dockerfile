FROM python:3.9

ADD Hello.py .

CMD ["python", "./Hello.py"]