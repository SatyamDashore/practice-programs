"""def outer_func():
    m='hi'
    def inner_func():
        print(m)
    return inner_func()
outer_func()"""

# Output---> hi

"""def outer_func():
    m='hi'
    def inner_func():
        print(m)
    return inner_func
my_func=outer_func()
my_func()
my_func()
my_func()"""

# Output---> 
"""
hi
hi
hi
"""

def outer_func(msg):
    m=msg    #This line can alsio be skipped by changin m to msg in innerfunc->print
    def inner_func():
        print(m)
    return inner_func
hi_func=outer_func("hi")
bye_func=outer_func("bye")

hi_func()
bye_func()

# Output--->
"""
hi
bye
"""