Notify Class:

class Notify(object):
def __init__(self, url=os.environ['NOTIFY_URL']):
    self.url = url

@decorators.retry((requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError))
def text(self, message, recipient_number):
    errors = Errors()
    url = "{0}/v1/texts".format(self.url)
    data = {
    "message": message,
    "recipient_number": recipient_number
    }
    r = requests.post(url, json=data)
    errors.handle_errors(r)
    return r.json()

@decorators.retry((requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError))
def send_emailv2(self, message, template, client):
    errors = Errors()
    payload = {
    "message": message,
    "template_name": template,
    "template_content": client
    }
    url = "{0}/v1/emails2".format(self.url)
    r = requests.post(url, json=payload)
    errors.handle_errors(r)
    return r.json()

Test.py file for Notify class:


import pytest
from pytest_mock import MockerFixture

import makeenv
from urllib.parse import urlparse

import orm
import errors

# Test __init__

def test_notify_init():
    notify= orm.Notify()
    url = "XXXXX"
    assert notify.url == url

@pytest.fixture
def mock_text_post(mocker):
    def post(url, json):
        r = urlparse(url) 
        if "xxxx" not in r.path:
            resp = {"message": "Data Not Found"}
            return mocker.Mock(status_code=404, json=lambda: resp)
        else:
            assert "xxxx/v1/texts" in r.path
            assert isinstance(json, dict)
            assert isinstance(json["message"],str)
            assert isinstance(json["recipient_number"],str)
            resp = json
            return mocker.Mock(status_code=200, json=lambda: resp)

    mocker.patch("orm.requests.post", post)

def test_notify_text(mocker,mock_text_post):
    message = "Dummy Message"
    recipient_number = "287836927" 

    # Error_case
    notify = orm.Notify()
    with pytest.raises(errors.DataNotFound) as exception_info:
        notify.text(message, recipient_number)
    assert "Data Not Found" in str(exception_info.value)

    #success_case
    notify = orm.Notify("xxxx")
    result = notify.text(message, recipient_number)
    Expected = {'message': 'Dummy Message', 'recipient_number': '287836927'}
    assert result == Expected
    
@pytest.fixture
def mock_send_emailv2_post(mocker):
    def post(url, json):
        r = urlparse(url) 
        if "xxxx" not in r.path:
            resp = {"message": "Data Not Found"}
            return mocker.Mock(status_code=404, json=lambda: resp)
        else:
            assert "xxxx/v1/emails2" in r.path
            assert isinstance(json, dict)
            assert isinstance(json["message"],str)
            assert isinstance(json["template_name"],str)
            assert isinstance(json["template_content"],dict)
            resp = json
            return mocker.Mock(status_code=200, json=lambda: resp)
        
    mocker.patch("orm.requests.post", post)
    
def test_notify_send_emailv2(mocker, mock_send_emailv2_post):
    message = "Dummy Message"
    template = "Dummy Template Name"
    client = {
    "id": "fake_client_id",
    "name": "Fake Client"
    }
    # Error_case
    notify = orm.Notify()
    with pytest.raises(errors.DataNotFound) as exception_info:
        notify.send_emailv2(message, template, client)
    assert "Data Not Found" in str(exception_info.value)
    
    #success_case
    notify = orm.Notify("xxxx")
    result = notify.send_emailv2(message, template, client)
    Expected = {'template_content': {'id': 'fake_client_id', 'name': 'Fake Client'},
    'template_name': 'Dummy Template', 
    'message': 'Dummy Message'}
    assert result == Expected
    
